import os
import operator

class Counter(dict):
	def __missing__(self,key):
		return 0

wordDict = Counter()

os.chdir("Input")
fileList = os.listdir('.')
for file in fileList:
	with open(file,'r') as fp:
		for line in fp:
			for word in line.split():
				wordDict[word.lower()] += 1
os.chdir("..")

sortedList = sorted(wordDict.items(),key=operator.itemgetter(1))

with open("wordFrequency.csv",'w+') as fp:
	fp.write("Word,Count\n")
	for elem in sortedList:
		fp.write(elem[0] +","+str(elem[1]) +"\n")


