import os
import operator

class Counter(dict):
	def __missing__(self,key):
		return 0

class Phrase(list):
	def __init__(self,length):
		self.length = length
		for i in range(length):
			self.append("")

	def __str__(self):
		retStr = self[self.length-1]
		if self[self.length-1] == "":
			notFullFlag = True
		else:
			notFullFlag = False

		for i in range(self.length-2,-1,-1):
			retStr += (" " + self[i])
			if self[i] == "":
				notFullFlag = True
		if notFullFlag:
			retStr = ""
		return retStr

	def add(self,val):
		for i in range(self.length-1,0,-1):
			self[i] = self[i-1]
		self[0] = val

phraseSize = input("What size phrase would you like? 0-9 only:")


phraseDict = Counter()
phrase = Phrase(int(phraseSize))

os.chdir("Input")
fileList = os.listdir('.')
for file in fileList:
	with open(file,'r') as fp:
		for line in fp:
			for word in line.split():
				phrase.add(word.lower())
				phraseDict[str(phrase)] += 1

os.chdir("..")

sortedList = sorted(phraseDict.items(),key=operator.itemgetter(1))

with open("phraseFrequency"+str(phraseSize)+".csv",'w+') as fp:
	fp.write("Phrase,Count\n")
	for elem in sortedList:
		fp.write(elem[0] +","+str(elem[1]) +"\n")


